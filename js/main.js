$(document).ready(function() {

	var owl = $("#slider");

	$("a[href='#top']").click(function() {
		$("html, body").animate({ scrollTop: 0 }, "slow");
		return false;
	});

	$("#slider").owlCarousel({
		items: 1,
		responsive: false,
		nav: true,
	});
});
